const createError = require("http-errors");
const express = require("express"),
  app = express(),
  port = 3000,
  exphbs = require("express-handlebars");
const bodyParser = require("body-parser");
var session = require("express-session");
var passport = require("passport");

//config handlebars
const hbs = exphbs.create({
  defaultLayout: "home",
  extname: "hbs"
});

app.use(
  session({
    secret: "User",
    saveUninitialized: true,
    resave: true
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.engine("hbs", hbs.engine);
app.set("view engine", "hbs");
app.use(express.static(__dirname + "/public"));

//Xử lí điều hướng trang chủ
app.get("/", (req, res) => {
  res.redirect("/home");
});
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

//Điều hướng về các controllers
app.use("/home", require("./public/controllers/homepage_controller")); //Trang chủ

app.use("/login", require("./public/controllers/login_controller")); //Controller xử lí POST đăng nhập

app.use("/signup", require("./public/controllers/signup_controller")); //Controller xử lí POST đăng ký

app.use(
  "/password-recovery",
  require("./public/controllers/password_recovery_controller.js") //Controller xử lí POST quên mật khẩu
);

app.use("/product", require("./public/controllers/product_info_controller")); //Trang xem chi tiết sản phẩm

app.use(
  "/account",
  require("./public/controllers/account_management_controller.js")
); //Trang quản lý tài khoản

app.use("/search", require("./public/controllers/search_controller.js"));

app.use("/logout", require("./public/controllers/logout_controller.js"));

require("./middlewares/http-errors")(app); //Middleware xử lí lỗi

app.listen(port, () => console.log(`Example app listening on port ${port}`));
