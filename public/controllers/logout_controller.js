const express = require("express");
const router = express.Router();

router.post("/", async (req, res) => {
  if (req.user === undefined) {
    res.redirect("/login");
  } else {
    req.logOut();
    res.redirect("/");
  }
});
module.exports = router;
