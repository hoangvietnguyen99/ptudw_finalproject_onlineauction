const express = require("express");
const router = express.Router();
const auctionProduct = require("../models/auction-product");
const cat = require("../models/category");
const dateformat = require("dateformat");
const timediff = require("timediff");

router.get("/", async (req, res) => {
  console.log(req.user);
  var allProducts1 = await auctionProduct.test(); //dau gia nhieu nhat
  var allProducts2 = await auctionProduct.test(); //gia hien tai cao nhat
  var allProducts3 = await auctionProduct.test(); //sap het han

  allProducts1 = allProducts1[0];
  allProducts2 = allProducts2[0];
  allProducts3 = allProducts3[0];

  const category = await cat.all();
  const types = await cat.allSubCategories();

  var categoryItem, typeItem;
  for (categoryItem of category) {
    categoryItem.types = [];
    for (typeItem of types) {
      if (typeItem.CatID === categoryItem.CatID) {
        categoryItem.types.push({
          TypeName: typeItem.TypeName,
          TypeID: typeItem.TypeID
        });
      }
    }
  }
  //5 sản phẩm giá bid cao nhất
  let limited = 0;
  let topBid = [];
  let aboutToExpire = [];
  let highestPrice = [];
  var sortedByPrice = allProducts2.sort((a, b) => {
    return b.CurrentPrice - a.CurrentPrice;
  });
  var sortedByTimes = [];
  var sortedByTimes = allProducts1.sort((a, b) => {
    return b.BidCount - a.BidCount;
  });

  //

  if (sortedByPrice.length > 5) {
    limited = 5;
  } else {
    limited = sortedByPrice.length;
  }
  for (let i = 0; i < limited; i++) {
    highestPrice[i] = sortedByPrice[i];
  }

  if (sortedByTimes.length > 5) {
    limited = 5;
  } else {
    limited = sortedByTimes.length;
  }
  for (let i = 0; i < limited; i++) {
    topBid[i] = sortedByTimes[i];
  }

  aboutToExpire = arrDateConvert(allProducts3);
  var arrTime = aboutToExpire[0].EndingDate.split(" ");
  var sortedByExpire = aboutToExpire.sort((a, b) => {
    var arrTime1 = a.EndingDate.split(" ");
    var arrTime2 = b.EndingDate.split(" ");
    let tempo1 = parseInt(arrTime1[0]) * 24 + parseInt(arrTime1[2]);
    let tempo2 = parseInt(arrTime2[0]) * 24 + parseInt(arrTime2[2]);
    if (tempo1 < tempo2) {
      return 1;
    } else {
      return -1;
    }
  });
  aboutToExpire = [];
  if (allProducts3.length > 5) {
    limited = 5;
  } else {
    limited = allProducts3.length;
  }
  for (let i = 0; i < limited; i++) {
    aboutToExpire[i] = sortedByExpire[i];
  }
  sortedByTimes = arrDateConvert(sortedByTimes);
  sortedByPrice = arrDateConvert(sortedByPrice);

  for (let i = 0; i < sortedByPrice.length; i++) {
    sortedByPrice[i].FullName = maskName(sortedByPrice[i].FullName);
  }
  for (let i = 0; i < sortedByTimes.length; i++) {
    sortedByTimes[i].FullName = maskName(sortedByTimes[i].FullName);
  }
  for (let i = 0; i < aboutToExpire.length; i++) {
    sortedByExpire[i].FullName = maskName(sortedByExpire[i].FullName);
  }
  if (req.user === undefined) {
    res.render("homepage", {
      isGuest: 1,
      Title: "Home",
      category: category,
      highestPrice: highestPrice,
      topBid: topBid,
      aboutToExpire: aboutToExpire
    });
  } else if (req.user.isSeller) {
    res.render("homepage", {
      isSeller: 1,
      Title: "Home",
      category: category,
      highestPrice: highestPrice,
      topBid: topBid,
      aboutToExpire: aboutToExpire
    });
  } else if (req.user.isBidder) {
    res.render("homepage", {
      isBidder: 1,
      Title: "Home",
      category: category,
      highestPrice: highestPrice,
      topBid: topBid,
      aboutToExpire: aboutToExpire
    });
  } else if (req.user.isAdmin) {
    res.redirect("/account/admin");
  }
});
function arrDateConvert(a) {
  for (item of a) {
    item.EndingDate = timediff(item.StartingDate, item.EndingDate);
    item.EndingDate =
      item.EndingDate.days + " ngày " + item.EndingDate.hours + " giờ";
    item.StartingDate = dateformat(item.StartingDate, "dd/mm/yyyy");
  }
  return a;
}
function maskName(a) {
  if (a == null) {
    return "Trống";
  }
  return (
    "*****************" + a[a.length - 3] + a[a.length - 2] + a[a.length - 1]
  );
}
module.exports = router;
