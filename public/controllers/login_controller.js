const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
var session = require("express-session");
const passport = require("passport"),
  LocalStrategy = require("passport-local").Strategy,
  accountM = require("../../public/models/account"),
  hash = require("../utils/hash");
const seller = require("../models/seller");
const bidder = require("../models/bidder");
const cat = require("../models/category");

router.get("/", async (req, res) => {
  if (req.user === undefined) {
    const category = await cat.all();
    const types = await cat.allSubCategories();

    var categoryItem, typeItem;
    for (categoryItem of category) {
      categoryItem.types = [];
      for (typeItem of types) {
        if (typeItem.CatID === categoryItem.CatID) {
          categoryItem.types.push({
            TypeName: typeItem.TypeName,
            TypeID: typeItem.TypeID
          });
        }
      }
    }

    res.render("login", {
      isGuest: 1,
      Title: "Đăng nhập",
      category: category
    });
  } else {
    res.redirect("/");
  }
});

router.use(
  session({
    secret: "User",
    saveUninitialized: true,
    resave: true
  })
);

router.use(
  bodyParser.urlencoded({
    extended: true
  })
);
router.use(bodyParser.json());

//Cấu hình passport
passport.serializeUser(function(user, done) {
  done(null, {
    id: user.AccID,
    isAdmin: user.isAdmin,
    isSeller: user.isSeller,
    isBidder: user.isBidder
  });
});

passport.deserializeUser(async (user, done) => {
  if (user.isAdmin) {
    done(null, user);
  }
  if (user.isSeller) {
    let [account, err] = await seller.byID(user.id);
    account.isSeller = 1;
    done(err, account);
  }
  if (user.isBidder) {
    let [account, err] = await bidder.byID(user.id);
    account.isBidder = 1;
    done(err, account);
  }
});

passport.use(
  new LocalStrategy(async (username, password, done) => {
    //Check admin
    let [user, err] = await accountM.getAdmin(username);
    if (user !== undefined) {
      let isValidUser = await hash.comparePassword(password, user.Password);
      if (isValidUser) {
        user.isAdmin = 1;
        return done(null, user);
      } else {
        return done(null, false, { message: "Sai mật khẩu!" });
      }
    }

    //Check seller
    [user, err] = await seller.byEmail(username);
    if (user !== undefined) {
      let isValidUser = await hash.comparePassword(password, user.Password);
      if (isValidUser) {
        user.isSeller = 1;
        return done(null, user);
      } else {
        return done(null, false, { message: "Sai mật khẩu!" });
      }
    }

    //Check bidder
    [user, err] = await accountM.byEmail(username);
    if (err) {
      return done(err);
    }
    if (user === undefined) {
      return done(null, false, { message: "Sai email" });
    }
    let isUser = await hash.comparePassword(password, user.Password);
    if (!isUser) {
      return done(null, false, { message: "Sai mật khẩu!" });
    }
    user.isBidder = 1;
    return done(null, user);
  })
);

/*router.use((req, res, next) => {
  res.locals.user = req.user;
});*/

router.post("/", async (req, res, next) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.render("login", {
        Title: "Đăng nhập",
        loginError: 1,
        isGuest: 1
      });
    }
    req.login(user, err => {
      if (err) {
        return next(err);
      }
      return res.redirect("/");
    });
  })(req, res, next);
});

module.exports = router;
