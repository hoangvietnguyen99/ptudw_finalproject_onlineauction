const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const fullTextSearch = require("full-text-search");
const mPro = require("../models/product");
const mProAuc = require("../models/auction-product");
const cat = require("../models/category");
const dateformat = require("dateformat");
const timediff = require("timediff");

const search = new fullTextSearch();
const perPage = 2;

let ResProduct = [];
let ResProductPerPage = [];
let indexResProduct = 0;
let CatName = [];
router.use(
  bodyParser.urlencoded({
    extended: true
  })
);
router.use(bodyParser.json());

//get all product to get all ProName

let searchAdd = [];
router.post("/", async (req, res) => {
  //search by Name
  const category = await cat.all();
  const types = await cat.allSubCategories();

  var categoryItem, typeItem;
  for (categoryItem of category) {
    categoryItem.types = [];
    for (typeItem of types) {
      if (typeItem.CatID === categoryItem.CatID) {
        categoryItem.types.push({
          TypeName: typeItem.TypeName,
          TypeID: typeItem.TypeID
        });
      }
    }
  }
  console.log(req.body);
  //console.log(`${req.body.searchOption}` == "Theo tên");
  const temp = `${req.body.searchContent}`;
  const typeSort = `${req.body.sortOption}`;
  const keyWord = temp.toUpperCase();
  console.log(keyWord);
  if (`${req.body.searchContent}`==''){
    ResProduct = await mProAuc.test();
    ResProduct = ResProduct[0];
  }
  if (`${req.body.searchOption}`== "Theo tên"){
    let Product = await mProAuc.test();
    Product = Product[0];
    for (let i=0;i<Product.length;i++){
      const objAdd = {
        ProID: Product[i].ProID,
        ProName: Product[i].ProName.toUpperCase()
      }
      searchAdd.push(search.add(objAdd));
    }
    //console.table(searchAdd);
  
    const results = search.search(keyWord);
    search.drop();
    searchAdd = [];
    //console.table(results);
    for (let i = 0;i<results.length;i++){
      for (let j =0 ;j <Product.length;j++){
        if (results[i].ProID==Product[j].ProID){
          ResProduct.push(Product[i]);
        }
      }
    }
  }else{
    //search by type 
    //obj will have 3 fields: CatID, CatName, TypeID
    
    const NameType1 = await mPro.getCatNameType1();
    const NameType2 = await mPro.getCatNameType2();
    // for (let i =0 ;i<NameType1.length;i++){
    //   CatName.push(NameType1[i].CatName);
    //   const tempo = await mPro.getTypeIDFromCatID(NameType1[i].CatID);
    //   console.log(tempo);
    //   const obj = {
    //     CatID: NameType1[i].CatID,
    //     CatName: NameType1[i].CatName,
    //     TypeID: null
    //   }
    //   searchAdd.push(search.add(obj));
    
    // }
    //console.table(NameType2);
    for (let i =0 ;i<NameType2.length;i++){
      const obj1 = {
        CatName: NameType2[i].TypeName.toUpperCase(),
        TypeID: NameType2[i].TypeID
      }
      searchAdd.push(search.add(obj1));
    } 
    const results = search.search(keyWord);
    search.drop();
    searchAdd = [];
    if (results.length == 0){
    }
    else{
      Product = await mProAuc.test();
      Product = Product[0];
      for (let i = 0 ; i < results.length; i++){
        for (let j = 0 ; j < Product.length; j++){
          if (results[i].TypeID == Product[j].TypeID){
            ResProduct.push(Product[j]);
          }
        }
      }
    }
  }
  ResProduct = arrDateConvert(ResProduct);
    for (let i =0 ;i<ResProduct.length;i++){
      ResProduct[i].FullName = maskName(ResProduct[i].FullName);
    }
  switch (typeSort){
    case 'type1':{
      console.log(typeSort);
      ResProduct.sort((a, b)=>{
        if (a.CurrentPrice < b.CurrentPrice){
          return 1;
        }else{
          return -1;
        }
      });
      break;
    }
    case 'type2':{
      ResProduct.sort((a, b)=>{
        if (a.CurrentPrice > b.CurrentPrice){
          return 1;
        }else{
          return -1;
        }
      });
      console.log(typeSort);
      break;
    }
    case 'type3':{
      ResProduct.sort((a, b)=>{
        var arrTime1 = a.EndingDate.split(" ");
        var arrTime2 = b.EndingDate.split(" ");
        let tempo1 = parseInt(arrTime1[0])*24 + parseInt(arrTime1[2]);
        let tempo2 = parseInt(arrTime2[0])*24 + parseInt(arrTime2[2]);
        if (tempo1 > tempo2){
            return 1;
        }else {return -1;}

      });
      console.log(typeSort);
      break;
    }
    case 'type4':{
      ResProduct.sort((a, b)=>{
        var arrTime1 = a.EndingDate.split(" ");
        var arrTime2 = b.EndingDate.split(" ");
        let tempo1 = parseInt(arrTime1[0])*24 + parseInt(arrTime1[2]);
        let tempo2 = parseInt(arrTime2[0])*24 + parseInt(arrTime2[2]);
        if (tempo1 < tempo2){
            return 1;
        }else {return -1;}

      });
      console.log(typeSort);
      break;
    }
  }
  //console.log(ResProduct);
  res.render("product-list", {
    ResProduct: ResProduct,
    Title: "Search results",
    ProductListName: "Kết quả tìm kiếm cho " + `"${req.body.searchContent}"`,
    category: category
  });
  ResProduct = [];
});
function arrDateConvert(a) {
  for (item of a) {
    item.EndingDate = timediff(item.StartingDate, item.EndingDate);
    item.EndingDate =
      item.EndingDate.days + " ngày " + item.EndingDate.hours + " giờ";
    item.StartingDate = dateformat(item.StartingDate, "dd/mm/yyyy");
  }
  return a;
};
function maskName(a){
  if (a ==null){
    return "Trống"
  }
  return "*****************"+a[a.length-3]+a[a.length-2]+a[a.length-1];
};
module.exports = router;
