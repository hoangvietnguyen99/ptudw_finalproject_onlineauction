const express = require("express");
const router = express.Router();
const account = require("../models/account");
const db = require("../utils/database");
const watchlist = require("../models/watch-list");
const bidder = require("../models/bidder");
const seller = require("../models/seller");
const pd = require("../models/product");
const ap = require("../models/auction-product");
const cat = require("../models/category");
const wts = require("../models/waiting-to-sell");
const dateformat = require("dateformat");
const timediff = require("timediff");
const hash = require("../utils/hash");

router.get("/", async (req, res) => {
  const category = await cat.all();
  const types = await cat.allSubCategories();

  var categoryItem, typeItem;
  for (categoryItem of category) {
    categoryItem.types = [];
    for (typeItem of types) {
      if (typeItem.CatID === categoryItem.CatID) {
        categoryItem.types.push({
          TypeName: typeItem.TypeName,
          TypeID: typeItem.TypeID
        });
      }
    }
  }

  if (req.user === undefined) {
    res.redirect("/login");
  } else if (req.user.isSeller) {
    //Get user's info
    let user = req.user;
    //Favorite list
    let watchlistinfo = await watchlist.byUserID(user.BidderID);
    watchlistinfo = arrDateConvert(watchlistinfo);

    //Bidding list
    const args = [user.BidderID];
    let biddingList = await db.call("proc_SanPhamDangDauGiaTheoBidderID", args);
    biddingList = arrDateConvert(biddingList[0]);
    //Won product list
    let wonProducts = await pd.getWonProductByUserID(user.BidderID);

    //My active products
    let myActiveProducts = await ap.getActiveBySeller(user.SellerID);
    myActiveProducts = arrDateConvert(myActiveProducts);

    //My finished products
    let myFinishedProducts = await ap.getFinishedBySeller(user.SellerID);
    myFinishedProducts = arrDateConvert(myFinishedProducts);

    //My ratings
    const myRatings = await account.getSellerFeedback(req.user.SellerID);
    user.RatedPoint = req.user.SellPoint / req.user.BeRated;
    console.log("rat" + myRatings);
    res.render("account-management", {
      isSeller: 1,
      user: user,
      watchlist: watchlistinfo,
      wonProducts: wonProducts,
      biddingList: biddingList,
      myActiveProducts: myActiveProducts,
      myFinishedProducts: myFinishedProducts,
      myRatings: myRatings,
      category: category,
      Title: "Quản lý tài khoản "
    });
  } else if (req.user.isBidder) {
    //Get user's info
    let user = req.user;
    //Favorite list
    let watchlistinfo = await watchlist.byUserID(user.BidderID);
    watchlistinfo = arrDateConvert(watchlistinfo);

    //Bidding list
    const args = [user.BidderID];
    let biddingList = await db.call("proc_SanPhamDangDauGiaTheoBidderID", args);
    biddingList = arrDateConvert(biddingList[0]);
    //Won product list
    let wonProducts = await pd.getWonProductByUserID(user.BidderID);

    const myRatings = await account.getBidderFeedback(req.user.BidderID);
    console.log(myRatings);
    user.RatedPoint = req.user.BidPoint / req.user.BeRated;
    res.render("account-management", {
      isBidder: 1,
      user: user,
      watchlist: watchlistinfo,
      wonProducts: wonProducts,
      biddingList: biddingList,
      myRatings: myRatings,
      category: category,
      Title: "Quản lý tài khoản"
    });
  } else if (req.user.isAdmin) {
    res.redirect("/account/admin");
  }
});

router.post("/request", async (req, res) => {
  const args = [req.user.AccID];
  try {
    await db.call("proc_NewWaitingToSell", args);
  } catch (err) {
    console.log(err);
    res.redirect("/account");
  }
  res.redirect("/account");
});

router.post("/changeinfo", async (req, res) => {
  if (req.body.FullName.length === 0 && req.body.Email.length === 0) {
    res.redirect("/account");
  }

  if (req.body.FullName.length !== 0) {
    await db.load(
      `UPDATE account SET FullName='${req.body.FullName}' WHERE AccID='${req.user.AccID}'`
    );
  }
  if (req.body.Email.length !== 0) {
    await db.load(
      `UPDATE account SET Email='${req.body.Email}' WHERE AccID='${req.user.AccID}'`
    );
  }
  res.redirect("/account");
});

router.post("/changePassword", async (req, res) => {
  if (
    req.body.newPassword.length === 0 ||
    req.body.newPasswordRepeat.length === 0 ||
    req.body.oldPassword === 0
  ) {
    res.redirect("/account");
  }

  //Kiem tra mat khau cu
  let isValidPassword = hash.comparePassword(
    req.body.oldPassword,
    req.user.Password
  );

  if (!isValidPassword) {
    //Mat khau cu khong dung
    res.redirect("/account");
  } else {
    //Dung mat khau cu
    if (req.body.newPassword !== req.body.newPasswordRepeat) {
      //Kiem tra nhap lai mat khau moi
      res.redirect("/account");
    } else {
      //UPDATE MAT KHAU
      req.body.newPassword = hash.getHashWithSalt(newPassword);
      await db.load(
        `UPDATE account SET Password='${req.body.newPassword}' WHERE AccID='${req.user.AccID}'`
      );

      res.redirect("/account");
    }
  }
});

router.get("/admin", async (req, res) => {
  const category = await cat.all();
  const types = await cat.allSubCategories();
  const waitingToSell = await wts.all();
  const accountList = await account.all();

  for (item of waitingToSell) {
    item.DateTime = dateformat(item.DateTime, "dd/mm/yyyy hh:mm:ss");
  }

  var categoryItem, typeItem;
  for (categoryItem of category) {
    categoryItem.types = [];
    for (typeItem of types) {
      if (typeItem.CatID === categoryItem.CatID) {
        categoryItem.types.push({
          TypeName: typeItem.TypeName,
          TypeID: typeItem.TypeID
        });
      }
    }
  }

  for (item of accountList) {
    const sellerID = await seller.byID(item.AccID);
    if (sellerID.length !== 0) {
      item.isSeller = 1;
    } else {
      item.isBidder = 1;
    }
  }
  console.log(accountList);

  //Get categories and types
  const categoriesAndTypes = await cat.getCategoryAndTypes();

  if (req.user === undefined) {
    res.redirect("/login");
  } else if (req.user.isSeller) {
    res.redirect("/");
  } else if (req.user.isBidder) {
    res.redirect("/");
  } else if (req.user.isAdmin) {
    res.render("admin-dashboard", {
      Title: "Admin",
      category: category,
      types: types,
      categoriesAndTypes: categoriesAndTypes,
      accountList: accountList,
      waitingToSell: waitingToSell,
      isAdmin: 1
    });
  }
});

router.post("/acceptrequest=:AccID", async (req, res) => {
  const AccID = req.params.AccID;
  //Set seller status
  try {
    await db.call("proc_NewSeller", [AccID]);
    await db.del("waitingtosell", "AccID", AccID);
  } catch (err) {
    console.log(err);
  }
  res.redirect("/account/admin");
});

router.post("/deleterequest=:AccID", async (req, res) => {
  const AccID = req.params.AccID;
  //Set seller status
  try {
    await db.del("waitingtosell", "AccID", AccID);
  } catch (err) {
    console.log(err);
  }
  res.redirect("/account/admin");
});

router.post("/downgradeseller=:AccID", async (req, res) => {
  const AccID = req.params.AccID;
  try {
    await db.del("seller", "AccID", AccID);
  } catch (error) {
    console.log(error);
  }
  res.redirect("/account/admin");
});

router.post("/upgradebidder=:AccID", async (req, res) => {
  const AccID = req.params.AccID;
  try {
    await db.call("proc_NewSeller", [AccID]);
  } catch (error) {
    console.log(error);
  }
  res.redirect("/account/admin");
});

function arrDateConvert(a) {
  for (item of a) {
    item.EndingDate = timediff(item.StartingDate, item.EndingDate);
    item.EndingDate =
      item.EndingDate.days + " ngày " + item.EndingDate.hours + " giờ";
    item.StartingDate = dateformat(item.StartingDate, "dd/mm/yyyy");
  }
  return a;
}
module.exports = router;
