const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const db = require("../utils/database");
const account = require("../models/account");
const hash = require("../utils/hash");
const cat = require("../models/category");

router.use(
  bodyParser.urlencoded({
    extended: true
  })
);
router.use(bodyParser.json());

router.get("/", async (req, res) => {
  if (req.user === undefined) {
    const category = await cat.all();
    const types = await cat.allSubCategories();

    var categoryItem, typeItem;
    for (categoryItem of category) {
      categoryItem.types = [];
      for (typeItem of types) {
        if (typeItem.CatID === categoryItem.CatID) {
          categoryItem.types.push({
            TypeName: typeItem.TypeName,
            TypeID: typeItem.TypeID
          });
        }
      }
    }
    res.render("signup", {
      isGuest: 1,
      Title: "Đăng nhập",
      category: category
    });
  } else {
    res.redirect("/");
  }
});

router.post("/", async (req, res) => {
  try {
    const user = {
      AccID: null,
      Email: req.body.signUpEmail,
      UserName: req.body.signUpEmail,
      Password: req.body.signUpPassword,
      PasswordConfirm: req.body.signUpPasswordConfirm,
      FullName: req.body.signUpName,
      Address: req.body.signUpAddress,
      PhoneNumber: null,
      DateOfBirth: null,
      Gender: null
    };
    const recaptcha = req.body["g-recaptcha-response"];

    let errorFlag = false;

    if (recaptcha === "") {
      res.render("signup", {
        Title: "Đăng ký",
        isGuest: 1,
        signupErrorCaptcha: 1
      });
    } else {
      if (user.Fullname === "") {
        errorFlag = true;
        console.log("name");
      }
      if (user.Email === "") {
        errorFlag = true;
        console.log("mail");
      }
      if (user.Password === "") {
        errorFlag = true;
        console.log("pass");
      }
      if (user.Password !== user.PasswordConfirm) {
        errorFlag = true;
        console.log("pass2");
      }
      if (errorFlag) {
        res.render("signup", {
          Title: "Đăng ký",
          isGuest: 1,
          signupError: 1
        });
      } else {
        //Kiểm tra trùng email

        let searchUser = await account.byEmail(user.Email);
        console.log(searchUser);
        if (searchUser.length !== 0) {
          res.render("signup", {
            Title: "Đăng ký",
            isGuest: 1,
            signupError: 1
          });
        } else {
          user.Password = await hash.getHashWithSalt(user.Password);
          await db.call("proc_NewAccount", [
            user.UserName,
            user.Email,
            user.Password,
            user.FullName,
            user.Address,
            "null",
            "null",
            "null"
          ]);
          res.redirect("/login");
        }
      }
    }
  } catch (err) {
    console.log(err);
  }

  //Kiểm tra và báo lỗi
  /* */
});

module.exports = router;
