const express = require("express");
const router = express.Router();
const mPro = require("../models/auction-product");
const bodyParser = require("body-parser");
const multer = require("multer");
const cat = require("../models/category");
const type = require("../models/type");
const db = require("../utils/database");
const pd = require("../models/product");
const seller = require("../models/seller");
const dateformat = require("dateformat");
const timediff = require("timediff");
const ap = require("../models/auction-product");
const fs = require("fs");
const nodemailer = require("nodemailer");

var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "onlineauctioncq2017@gmail.com",
    pass: "Onlineauction"
  }
});

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./public/assets/ProductImg/");
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});

var upload = multer({ storage: storage });

router.use(
  bodyParser.urlencoded({
    extended: true
  })
);
router.use(bodyParser.json());

router.get("/id=:ProID", async (req, res) => {
  /*var mailOptions = {
    from: "onlineauctioncq2017@gmail.com",
    to: "blkgn157@gmail.com",
    subject: "Sending Email using Node.js",
    text: "That was easy!"
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });*/

  const ProID = req.params.ProID;

  const category = await cat.all();
  const types = await cat.allSubCategories();

  var categoryItem, typeItem;
  for (categoryItem of category) {
    categoryItem.types = [];
    for (typeItem of types) {
      if (typeItem.CatID === categoryItem.CatID) {
        categoryItem.types.push({
          TypeName: typeItem.TypeName,
          TypeID: typeItem.TypeID
        });
      }
    }
  }

  let Product = await mPro.getProductByID(ProID);
  let Seller = await mPro.getSeller(ProID);
  let TopBidder = await mPro.getTopBidder(ProID);
  let BidHistory = await mPro.getBidHistory(ProID);
  BidHistory = arrDateConverBidHistory(BidHistory);

  let relatedProducts = await mPro.test();
  relatedProducts = relatedProducts[0];
  Product = arrDateConvert(Product);
  Product = Product[0];
  let SuggestedPrice = Product.CurrentPrice + Product.PriceStep;
  var relatedProductsResult = [];
  let limited = 0;
  if (relatedProducts.length > 5) {
    limited = 5;
  } else {
    limited = relatedProducts.length;
  }
  for (let i = 0; i < limited; i++) {
    relatedProductsResult[i] = relatedProducts[i];
  }
  relatedProductsResult = arrDateConvert(relatedProductsResult);
  for (let i = 0; i < relatedProductsResult.length; i++) {
    relatedProductsResult[i].FullName = maskName(
      relatedProductsResult[i].FullName
    );
  }

  Seller[0].FullName = maskName(Seller[0].FullName);
  if (TopBidder.length != 0) {
    TopBidder[0].FullName = maskName(TopBidder[0].FullName);
  }

  if (req.user === undefined) {
    res.render("product-info", {
      Product: Product,
      Seller: Seller[0],
      TopBidder: TopBidder[0],
      BidHistory: BidHistory,
      RelatedProducts: relatedProductsResult,
      isGuest: 1,
      category: category,
      Title: Product.ProName
    });
  } else if (req.user.isSeller) {
    let isOwner = 0;
    if (Product.SellerID === req.user.SellerID) {
      isOwner = 1;
    }
    res.render("product-info", {
      isSeller: 1,
      Product: Product,
      Seller: Seller[0],
      SuggestedPrice: SuggestedPrice,
      TopBidder: TopBidder[0],
      BidHistory: BidHistory,
      isOwner: isOwner,
      RelatedProducts: relatedProductsResult,
      category: category,
      Title: Product.ProName
    });
  } else if (req.user.isBidder) {
    res.render("product-info", {
      isBidder: 1,
      Product: Product,
      Seller: Seller[0],
      SuggestedPrice: SuggestedPrice,
      TopBidder: TopBidder[0],
      BidHistory: BidHistory,
      RelatedProducts: relatedProductsResult,
      category: category,
      Title: Product.ProName
    });
  }
});

router.get("/ptypeid=:PTypeID/page=:page", async (req, res) => {
  const category = await cat.all();
  const types = await cat.allSubCategories();
  //getTypeID
  //const page = 1;
  const page = parseInt(req.params.page);
  //console.log(page);
  const TypeID = req.params.PTypeID;
  let prevPage = page - 1;
  let nextPage = page + 1;
  const perpage = 2;

  let ProductListName = await pd.getTypeNameByTypeID(TypeID);
  console.log(ProductListName);
  ProductListName = ProductListName[0].TypeName;

  //console.log(TypeID);
  let ResProduct = [];
  let currentPagePro = [];
  if (!page) {
    page = 1;
  }
  let ListProID = await mPro.test();
  ListProID = ListProID[0];
  //console.log(ListProID);
  for (let i = 0; i < ListProID.length; i++) {
    if (ListProID[i].TypeID == TypeID) {
      ResProduct.push(ListProID[i]);
    }
  }
  var categoryItem, typeItem;
  for (categoryItem of category) {
    categoryItem.types = [];
    for (typeItem of types) {
      if (typeItem.CatID === categoryItem.CatID) {
        categoryItem.types.push({
          TypeName: typeItem.TypeName,
          TypeID: typeItem.TypeID
        });
      }
    }
  }
  for (let i = (page - 1) * perpage; i < page * perpage; i++) {
    if (ResProduct[i]) {
      currentPagePro.push(ResProduct[i]);
    } else {
      nextPage = 0;
      break;
    }
  }
  if (
    page === parseInt(ResProduct.length / perpage) &&
    parseInt(ResProduct.length % perpage) === 0
  ) {
    nextPage = 0;
  }
  if (page === 1) {
    prevPage = 0;
  }
  //console.log(ResProduct);
  ResProduct = arrDateConvert(ResProduct);
  for (let i = 0; i < ResProduct.length; i++) {
    ResProduct[i].FullName = maskName(ResProduct[i].FullName);
  }
  res.render("product-list", {
    Title: "Category",
    ResProduct: currentPagePro,
    category: category,
    ProductListName: ProductListName,
    page: page,
    PrevPage: prevPage,
    NextPage: nextPage,
    TypeID: TypeID
  });
});

router.get("/add", async (req, res) => {
  const category = await cat.all();
  const types2 = await cat.allSubCategories();

  var categoryItem, typeItem;
  for (categoryItem of category) {
    categoryItem.types = [];
    for (typeItem of types2) {
      if (typeItem.CatID === categoryItem.CatID) {
        categoryItem.types.push({
          TypeName: typeItem.TypeName,
          TypeID: typeItem.TypeID
        });
      }
    }
  }
  const types = await type.all();

  if (req.user === undefined) {
    res.redirect("/login");
  } else if (req.user.isSeller) {
    res.render("upload-product", {
      isSeller: 1,
      Title: "Đăng bài",
      category: category,
      ProductTypes: types
    });
  } else if (req.user.isBidder) {
    res.redirect("/");
  }
});

router.post("/add", upload.array("Images", 3), async (req, res) => {
  //Get uploaded infos
  let TypeID = req.body.Type.substring(0, req.body.Type.indexOf(" "));
  let files = req.files;
  let id;
  let router = res;
  const productToAdd = [
    TypeID,
    req.body.Name,
    req.body.BuyoutPrice,
    req.body.Description,
    req.user.SellerID
  ];

  //Upload product and images

  await db.call("proc_NewProduct", productToAdd);
  pd.getIDbyName(req.body.Name).then(async results => {
    let ProID = results[0].ProID;
    id = ProID;
    let today = new Date();
    let expirationDate = today.addDays(5);
    const auctionProduct = [
      ProID,
      dateformat(today, "yyyy-mm-dd hh:mm:ss"),
      dateformat(expirationDate, "yyyy-mm-dd hh:mm:ss"),
      req.body.StartingPrice,
      req.body.PriceStep
    ];

    await db.call("proc_NewAuctionProduct", auctionProduct);

    //Change image directory
    fs.mkdir(files[0].destination + `${ProID}/`, function(err) {
      if (err) console.log("ERROR: " + err);
    });

    for (let i = 0; i < 3; i++) {
      if (!files[i]) {
        break;
      }
      fs.rename(
        files[i].destination + files[i].originalname,
        files[i].destination + `${ProID}/` + `${i + 1}.jpg`,
        function(err) {
          if (err) console.log("ERROR: " + err);
        }
      );
    }
    router.redirect("/product/id=" + ProID);
  });
});

Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

router.post("/rateseller&proid=:ProID&sellerid=:SellerID", async (req, res) => {
  const ProID = req.params.ProID;
  const SellerID = req.params.SellerID;
  const BidderID = req.user.BidderID;
  const Comment = req.body.Comment;
  let RatePoint;
  if (req.body.BuyerReaction === "Thích") {
    RatePoint = 1;
  } else {
    RatePoint = -1;
  }
  const FeedBackToSeller = [ProID, BidderID, SellerID, Comment, RatePoint];

  try {
    await db.call("proc_NewFeedbackToSeller", FeedBackToSeller);
  } catch (err) {
    console.log(err);
  }
  res.redirect("/account");
});

router.post("/ratebidder&proid=:ProID&bidderid=:BidderID", async (req, res) => {
  const ProID = req.params.ProID;
  const BidderID = req.params.BidderID;
  const SellerID = req.user.SellerID;
  const Comment = req.body.Comment;
  let RatePoint;
  if (req.body.BuyerReaction === "Thích") {
    RatePoint = 1;
  } else {
    RatePoint = -1;
  }
  const FeedBackToBidder = [ProID, BidderID, SellerID, Comment, RatePoint];

  try {
    await db.call("proc_NewFeedbackToBidder", FeedBackToBidder);
  } catch (err) {
    console.log(err);
  }
  res.redirect("/account");
});

router.post("/addcategory", async (req, res) => {
  const CatName = req.body.CatName;
  const TypeName = req.body.TypeName;

  if (CatName === "" || TypeName === "") {
    res.redirect("/account/admin");
  }

  //Check if cat is already defined
  let dbCat;
  try {
    dbCat = await cat.getCategoryByName(CatName);
  } catch (err) {
    console.log(err);
  }

  //Nếu đã có category => Gọi thủ tục add type
  if (dbCat[0]) {
    try {
      await db.call("proc_NewType", [dbCat[0].CatID, TypeName]);
    } catch (err) {
      console.log(err);
    }
  }
  //Chưa có category =>Gọi thủ tục add category sau đó add type
  else {
    try {
      await db.call("proc_NewCategory", [CatName]); //Tạo cat mới
      const insertedCat = await cat.getCategoryByName(CatName);

      //Tạo type mới
      await db.call("proc_NewType", [insertedCat[0].CatID, TypeName]);
    } catch (err) {
      console.log(err);
    }
  }
  res.redirect("/account/admin");
});

router.post("/edittype&typeid=:TypeID", async (req, res) => {
  const CatName = req.body.CatName;
  const TypeName = req.body.TypeName;
  const TypeID = req.params.TypeID;

  if (CatName === "" || TypeName === "") {
    res.redirect("/account/admin");
  }

  //Check if cat is already defined
  let dbCat;
  try {
    dbCat = await cat.getCategoryByName(CatName);
  } catch (err) {
    console.log(err);
  }

  console.log(dbCat);
  //Nếu đã có category => Gọi thủ tục edit type
  if (dbCat[0]) {
    try {
      await db.call("proc_UpdateType", [TypeID, TypeName, dbCat[0].CatID]);
    } catch (err) {
      console.log(err);
    }
  }
  //Chưa có category =>Gọi thủ tục add category sau đó add type
  else {
    try {
      await db.call("proc_NewCategory", [CatName]); //Tạo cat mới
      const insertedCat = await cat.getCategoryByName(CatName);

      //Tạo type mới
      await db.call("proc_UpdateType", [
        TypeID,
        TypeName,
        insertedCat[0].CatID
      ]);
    } catch (err) {
      console.log(err);
    }
  }
  res.redirect("/account/admin");
});

router.post("/deletetype&typeid=:TypeID", async (req, res) => {
  const TypeID = req.params.TypeID;
  try {
    await db.del("type", "TypeID", TypeID);
  } catch (err) {
    console.log(err);
  }
  res.redirect("/account/admin");
});

router.post("/save&id=:ProID", async (req, res) => {
  const ProID = req.params.ProID;
  const BidderID = req.user.BidderID;
  await db.call("proc_NewWatchList", [ProID, BidderID]);
  res.redirect(`/product/id=${ProID}`);
});

router.post("/updateDes&id=:ProID", async (req, res) => {
  const ProID = req.params.ProID;
  const oldDes = await pd.getDesFromProID(ProID);
  var d = new Date();
  const editText = oldDes[0].Description + d + req.body.addedDescription;
  const resu = await pd.updateDesciption(editText, ProID);
  res.redirect(`/product/id=${ProID}`);
});
router.post("/newmanualbid&id=:ProID", async (req, res) => {
  const ProID = req.params.ProID;
  if (req.user.RatedPoint < 8) {
    res.redirect(`/product/id=${ProID}`);
  }
  const BidderID = req.user.BidderID;
  const newPriceManual = req.body.newPriceManual;
  const Product = await ap.getProductByID(ProID);
  const Seller = await seller.bySellerID(Product[0].SellerID);

  try {
    await db.call("proc_NewBiddedReport", [ProID, BidderID, newPriceManual]);

    var mailOptions = {
      from: "onlineauctioncq2017@gmail.com",
      to: req.user.Email,
      subject: "Thông báo đấu giá thành công!",
      text:
        "Đấu giá cho sản phẩm " +
        Product.ProName +
        " với mức đấu giá " +
        newPriceManual +
        " đã được chấp nhận!"
    };

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });

    var mailOptions2 = {
      from: "onlineauctioncq2017@gmail.com",
      to: Seller.Email,
      subject: "Thông báo đấu giá mới",
      text:
        "Sản phẩm " +
        Product.ProName +
        " vừa nhận một lượt đấu giá với mức giá " +
        newPriceManual +
        " đồng!"
    };

    transporter.sendMail(mailOptions2, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });

    res.redirect(`/product/id=${ProID}`);
  } catch (error) {
    res.redirect(`/product/id=${ProID}`);
  }
});
router.post("/newautobid&id=:ProID", async (req, res) => {
  const ProID = req.params.ProID;
  const BidderID = req.user.BidderID;
  const newPriceAuto = req.body.newPriceAuto;
  try {
    await db.call("proc_NewAutoBid", [BidderID, ProID, newPriceAuto]);
    res.redirect(`/product/id=${ProID}`);
  } catch (error) {
    res.redirect(`/product/id=${ProID}`);
  }
});
function arrDateConverBidHistory(a) {
  for (item of a) {
    item.BidDate = dateformat(item.StartingDate, "dd/mm/yyyy");
  }
  return a;
}
function arrDateConvert(a) {
  for (item of a) {
    item.EndingDate = timediff(item.StartingDate, item.EndingDate);
    item.EndingDate =
      item.EndingDate.days + " ngày " + item.EndingDate.hours + " giờ";
    item.StartingDate = dateformat(item.StartingDate, "dd/mm/yyyy");
  }
  return a;
}

function maskName(a) {
  if (a == null) {
    return "Trống";
  }
  return (
    "*****************" + a[a.length - 3] + a[a.length - 2] + a[a.length - 1]
  );
}
module.exports = router;
