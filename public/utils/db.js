const mysql = require("mysql");

function createConnection() {
  return mysql.createConnection({
    host: "localhost",
    port: "3306",
    user: "db_admin",
    password: "db_admin_pw",
    database: "OnlineAuction"
  });
}

exports.load = sql => {
  return new Promise((resole, reject) => {
    const con = createConnection();
    con.connect(err => {
      if (err) {
        reject(err);
      }
    });
    con.query(sql, (error, results, fields) => {
      if (error) {
        reject(error);
      }
      resole(results);
    });
    con.end();
  });
};
