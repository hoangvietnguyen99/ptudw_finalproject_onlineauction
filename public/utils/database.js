const mysql = require("mysql");

function createConnection() {
  return mysql.createConnection({
    host: "localhost",
    port: "8889",
    user: "root",
    password: "1234",
    database: "OnlineAuction"
  });
}

exports.load = sql => {
  return new Promise((resolve, reject) => {
    const con = createConnection();
    con.connect(err => {
      if (err) {
        reject(err);
      }
    });
    con.query(sql, (error, results, fields) => {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
    con.end();
  });
};

exports.del = (tbName, idField, id) => {
  return new Promise((resolve, reject) => {
    const con = createConnection();
    con.connect(err => {
      if (err) {
        reject(err);
      }
    });
    let sql = "DELETE FROM ?? WHERE ?? = ?";
    const params = [tbName, idField, id];
    sql = mysql.format(sql, params);
    con.query(sql, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        resolve(results.affectedRows);
      }
    });
    con.end();
  });
};

exports.update = (tbName, idField, entity) => {
  return new Promise((resolve, reject) => {
    const con = createConnection();
    con.connect(err => {
      if (err) {
        reject(err);
      }
    });
    const id = entity[idField];
    delete entity[idField];
    let sql = `UPDATE ${tbName} SET ? WHERE ${idField} = ${id}`;
    con.query(sql, entity, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        resolve(results.changedRows);
      }
    });
    con.end();
  });
};

exports.add = (tbName, entity) => {
  return new Promise((resolve, reject) => {
    const con = createConnection();
    con.connect(err => {
      if (err) {
        reject(err);
      }
    });
    let sql = `INSERT INTO ${tbName} SET ?`;
    con.query(sql, entity, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        resolve(results.changedRows);
      }
    });
    con.end();
  });
};

exports.call = (proc, args) => {
  return new Promise((resolve, reject) => {
    const con = createConnection();
    con.connect(err => {
      if (err) {
        reject(err);
      }
    });
    var finalArgs = args.join(",");
    let sql = `CALL ${proc}(`;
    for (let item of args) {
      if (item === "null") {
        sql = sql + "NULL" + ",";
      } else {
        sql = sql + "'" + item + "'" + ",";
      }
    }
    sql = sql.substring(0, sql.length - 1);
    sql = sql + ")";
    console.log(sql);

    con.query(sql, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        resolve(results);
      }
    });
    con.end();
  });
};
