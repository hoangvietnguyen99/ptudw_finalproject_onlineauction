const sha = require("sha.js"),
  hashLength = 64;

async function hashWithSalt(str, salt) {
  const preHash = str + salt;
  const hash = sha("sha256")
    .update(preHash)
    .digest("hex");
  const pwHash = hash + salt;
  return pwHash;
}

module.exports = {
  getHashWithSalt: async str => {
    const salt = Date.now().toString(16);
    const pwHash = await hashWithSalt(str, salt);
    return pwHash;
  },

  comparePassword: async (pwIn, pwSaved) => {
    const salt = pwSaved.substring(pwSaved.length, hashLength);
    const pwHash = await hashWithSalt(pwIn, salt);

    console.log(pwHash);

    return pwHash === pwSaved;
  }
};
