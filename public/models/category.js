const db = require("../utils/database");
const tbName = "category";
const tbName2 = "type";

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM ${tbName}`;
    const rows = await db.load(sql);
    return rows;
  },
  allSubCategories: async () => {
    const sql = `SELECT * FROM ${tbName2}`;
    const rows = await db.load(sql);
    return rows;
  },
  getCategoryAndTypes: async () => {
    const sql = `SELECT * FROM ${tbName2} as type, ${tbName} as category WHERE ${tbName2}.CatID = category.CatID`;
    const rows = await db.load(sql);
    return rows;
  },
  getCategoryByName: async name => {
    const sql = `SELECT * FROM ${tbName} WHERE ${tbName}.CatName = '${name}'`;
    const rows = await db.load(sql);
    return rows;
  }
};
