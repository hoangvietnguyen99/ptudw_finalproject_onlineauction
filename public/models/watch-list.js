const db = require("../utils/database");
const tbName = "watchlist";
const tbName2 = "product";
const tbName3 = "auctionproduct";
const tbName4 = "bidder";
const tbName5 = "account";

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM ${tbName}`;
    const rows = await db.load(sql);
    return rows;
  },

  byUserID: async id => {
    const sql = `SELECT * FROM ${tbName} as watchlist, ${tbName2} as product, ${tbName3} as auctionproduct, ${tbName4} as bidder,${tbName5} as account 
    where watchlist.BidderID = "${id}" ANd watchlist.ProID = product.ProID and watchlist.ProID = auctionproduct.ProID and watchlist.BidderID = bidder.BidderID and bidder.AccID = account.AccID`;
    const rows = await db.load(sql);
    return rows;
  }
};
