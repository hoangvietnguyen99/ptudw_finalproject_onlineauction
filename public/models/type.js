const db = require("../utils/database");
const tb = "type";

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM ${tb}`;
    const rows = await db.load(sql);
    return rows;
  }
};
