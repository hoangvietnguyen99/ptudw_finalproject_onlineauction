const db = require("../utils/database");
const tbName = "product";
const tbName2 = "account";
const tbName3 = "seller";
const tbName4 = "auctionproduct";
const tbName5 = "Category";
const tbName6 = "Type";
module.exports = {
  all: async () => {
    const sql = `SELECT * FROM ${tbName}`;
    const rows = await db.load(sql);
    return rows;
  },

  byID: async id => {
    const sql = `SELECT * FROM ${tbName} as product, ${tbName2} as account, ${tbName3} as seller 
    WHERE product.ProID='${id}' and product.SellerID = account.AccID and account.AccID = seller.SellerID `;
    const rows = await db.load(sql);
    return rows;
  },
  allByPTypeID: async id => {
    const sql = `SELECT * FROM ${tbName} WHERE TypeID='${id}'`;
    const rows = await db.load(sql);
    return rows;
  },

  getIDbyName: async name => {
    const sql = `SELECT * FROM ${tbName} WHERE ProName='${name}'`;
    const rows = await db.load(sql);
    return rows;
  },

  getWonProductByUserID: async id => {
    const sql = `SELECT * FROM ${tbName} as product, ${tbName4} as auctionproduct, ${tbName3} as seller, ${tbName2} as account  
    WHERE auctionproduct.isFinished=1 AND auctionproduct.CurrentTopBidder = '${id}'
    AND auctionproduct.ProID = product.ProID AND product.SellerID = seller.SellerID and seller.AccID = account.AccID`;
    const rows = await db.load(sql);
    return rows;
  },
  getCatNameType1: async () => {
    const sql = `SELECT * FROM ${tbName5}`;
    const rows = await db.load(sql);
    return rows;
  },
  getCatNameType2: async () => {
    const sql = `SELECT * FROM ${tbName6}`;
    const rows = await db.load(sql);
    return rows;
  },
  getTypeIDFromCatID: async CatID => {
    const sql = `SELECT * FROM ${tbName6} WHERE type.CatID = '${CatID}'`;
    const rows = await db.load(sql);
    return rows;
  },
  getTypeNameByTypeID: async TypeID => {
    const sql = `SELECT * FROM ${tbName6} WHERE TypeID = '${TypeID}'`;
    const rows = await db.load(sql);
    return rows;
  },
  getTypeIDFromCatID: async CatID => {
    const sql = `SELECT * FROM ${tbName6} WHERE type.CatID = '${CatID}'`;
    const rows = await db.load(sql);
    return rows;
  },
  getDesFromProID: async ProID => {
    const sql = `SELECT Description FROM ${tbName} WHERE ProID = '${ProID}'`;
    const rows = await db.load(sql);
    return rows;
  },
  updateDesciption: async (newDes, ProID) => {
    const sql = `UPDATE ${tbName} SET Description = '${newDes}' WHERE ProID = '${ProID}' `;
    const rows = await db.load(sql);
    return rows;
  }
};
