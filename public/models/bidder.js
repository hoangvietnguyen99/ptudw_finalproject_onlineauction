const db = require("../utils/database");
const tbName = "bidder";
const tbName2 = "account";

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM ${tbName}`;
    const rows = await db.load(sql);
    return rows;
  },

  byID: async id => {
    const sql = `SELECT * FROM ${tbName}, ${tbName2}  WHERE ${tbName}.AccID=${tbName2}.AccID AND ${tbName2}.AccID='${id}'`;
    const rows = await db.load(sql);
    return rows;
  }
};
