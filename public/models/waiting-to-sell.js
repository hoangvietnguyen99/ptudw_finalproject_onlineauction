const tbName = "waitingtosell";
const tbName2 = "account";
const db = require("../utils/database");

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM ${tbName} as waitingtosell, ${tbName2} as account WHERE waitingtosell.AccID = account.AccID`;
    const rows = await db.load(sql);
    return rows;
  }
};
