const db = require("../utils/database");
const tbName = "seller";
const tbName2 = "account";
const tbName3 = "bidder";

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM ${tbName}`;
    const rows = await db.load(sql);
    return rows;
  },

  byID: async id => {
    const sql = `SELECT * FROM ${tbName}, ${tbName2}, ${tbName3}  WHERE ${tbName2}.AccID=${tbName}.AccID AND ${tbName}.AccID='${id}' AND ${tbName2}.AccID = ${tbName3}.AccID`;
    const rows = await db.load(sql);
    return rows;
  },
  bySellerID: async id => {
    const sql = `SELECT * FROM ${tbName} as seller, ${tbName2} as account, ${tbName3} as bidder  WHERE account.AccID=seller.AccID AND seller.SellerID='${id}' AND account.AccID = bidder.AccID`;
    const rows = await db.load(sql);
    return rows;
  },
  byEmail: async email => {
    const sql = `SELECT * FROM ${tbName} as seller, ${tbName2} as account WHERE seller.AccID = account.AccID AND account.Email='${email}'`;
    const rows = await db.load(sql);
    return rows;
  }
};
