const db = require("../utils/database");
const tbName1 = "product",
  tbName2 = "auctionproduct",
  tbName3 = "seller",
  tbName4 = "account",
  tbName5 = "biddedreport",
  tbName6 = "bidder";

module.exports = {
  test: async () => {
    const rows = await db.load(`CALL proc_NeedToBid()`);
    return rows;
  },
  all: async () => {
    const sql = `SELECT * FROM ${tbName1} as product, ${tbName2} as auctionproduct, ${tbName4} as account, ${tbName6} as bidder 
    WHERE product.ProID = auctionproduct.ProID AND auctionproduct.isFinished=0 AND 
    auctionproduct.CurrentTopBidder = bidder.BidderID AND bidder.AccID = account.AccID`;
    const rows = await db.load(sql);
    return rows;
  },

  getProductByID: async id => {
    const sql = `SELECT * FROM ${tbName1} as product, ${tbName2} as auctionproduct
    WHERE product.ProID = '${id}' AND product.ProID = auctionproduct.ProID`;
    const rows = await db.load(sql);
    return rows;
  },

  getProductByTypeID: async TypeID => {
    const sql = `SELECT * FROM ${tbName1} as product, ${tbName2} as auctionproduct 
    WHERE product.TypeID = '${TypeID}' AND product.ProID = auctionproduct.ProID`;
    const rows = await db.load(sql);
    return rows;
  },

  getSeller: async id => {
    const sql = `SELECT * FROM ${tbName1} as product, ${tbName3} as seller, ${tbName4} as account
    WHERE product.ProID='${id}' AND product.SellerID = seller.SellerID AND seller.AccID = account.AccID`;
    const rows = await db.load(sql);
    return rows;
  },

  getTopBidder: async id => {
    const sql = `SELECT * FROM ${tbName2} as auctionproduct, ${tbName6} as currenttopbidder, ${tbName4} as account 
    WHERE auctionproduct.ProID='${id}' AND auctionproduct.CurrentTopBidder = currenttopbidder.BidderID AND currenttopbidder.AccID = account.AccID`;
    const rows = await db.load(sql);
    return rows;
  },

  getBidHistory: async id => {
    const sql = `SELECT * FROM ${tbName5} as biddedreport, ${tbName1} as product, ${tbName4} as account, ${tbName6} as bidder WHERE biddedreport.ProID = '${id}' AND biddedreport.ProID = product.ProID AND biddedreport.BidderID = bidder.BidderID AND bidder.AccID = account.AccID`;
    const rows = await db.load(sql);
    return rows;
  },

  getActiveBySeller: async sellerid => {
    const sql = `SELECT * FROM ${tbName1} as product, ${tbName2} as auctionproduct, ${tbName4} as account, ${tbName6} as bidder 
    WHERE product.SellerID = '${sellerid}' AND auctionproduct.isFinished=0 AND product.ProID = auctionproduct.ProID AND auctionproduct.isFinished=0 AND 
    auctionproduct.CurrentTopBidder = bidder.BidderID AND bidder.AccID = account.AccID`;
    const rows = await db.load(sql);
    return rows;
  },
  getFinishedBySeller: async sellerid => {
    const sql = `SELECT * FROM ${tbName1} as product, ${tbName2} as auctionproduct, ${tbName4} as account, ${tbName6} as bidder 
    WHERE product.SellerID = '${sellerid}' AND auctionproduct.isFinished=1 AND product.ProID = auctionproduct.ProID AND auctionproduct.isFinished=0 AND 
    auctionproduct.CurrentTopBidder = bidder.BidderID AND bidder.AccID = account.AccID`;
    const rows = await db.load(sql);
    return rows;
  }
};
