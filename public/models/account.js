const db = require("../utils/database");
const tbName = "account";

module.exports = {
  all: async () => {
    const sql = `SELECT * FROM ${tbName}`;
    const rows = await db.load(sql);
    return rows;
  },
  byID: async id => {
    const sql = `SELECT * FROM ${tbName} WHERE AccID='${id}'`;
    const rows = await db.load(sql);
    return rows;
  },
  byEmail: async email => {
    const sql = `SELECT * FROM ${tbName} WHERE Email='${email}'`;
    const rows = await db.load(sql);
    return rows;
  },
  getAdmin: async email => {
    const sql = `SELECT * FROM admin WHERE UserName='${email}'`;
    const rows = await db.load(sql);
    return rows;
  },
  getSellerFeedback: async sellerid => {
    const sql = `SELECT * FROM FeedbackToSeller WHERE SellerID='${sellerid}'`;
    const rows = await db.load(sql);
    return rows;
  },
  getBidderFeedback: async bidderid => {
    const sql = `SELECT * FROM FeedbackToBidder WHERE BidderID='${bidderid}'`;
    const rows = await db.load(sql);
    return rows;
  }
};
