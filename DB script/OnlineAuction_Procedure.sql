/*procedure*/
DELIMITER $$
CREATE PROCEDURE `proc_NewSeller` (_accid CHAR(6))
BEGIN
	DECLARE _id INT;
	DECLARE _sellerid CHAR(6);
	SET _id = IFNULL((SELECT MAX(ID) FROM `OnlineAuction`.`Seller`),0) + 1;
    SET _sellerid = CONCAT("SEL",CAST(_id DIV 100 AS CHAR),CAST(MOD(_id,100) DIV 10 AS CHAR),CAST(MOD(_id, 10) AS CHAR));
	IF (SELECT AccID FROM `OnlineAuction`.`Seller` WHERE AccID = _accid) IS NULL THEN
		INSERT INTO `OnlineAuction`.`Seller` (`ID`,`SellerID`,`AccID`)
		VALUES
		(_id,_sellerid,_accid);
	ELSE
		SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "That account is already a seller!";
    END IF;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewBidder` (_accid CHAR(6))
BEGIN
	DECLARE _id INT;
	DECLARE _bidderid CHAR(6);
	SET _id = IFNULL((SELECT MAX(ID) FROM `OnlineAuction`.`Bidder`),0) + 1;
    SET _bidderid = CONCAT("BID",CAST(_id DIV 100 AS CHAR),CAST(MOD(_id,100) DIV 10 AS CHAR),CAST(MOD(_id, 10) AS CHAR));
	IF (SELECT AccID FROM `OnlineAuction`.`Bidder` WHERE AccID = _accid) IS NULL THEN
		INSERT INTO `OnlineAuction`.`Bidder` (`ID`,`BidderID`,`AccID`)
		VALUES
		(_id,_bidderid,_accid);
	ELSE
		SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "That account is already a bidder!";
    END IF;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewAccount` (_email text, _username text, _password text, _fullname text, _address text, _phonenumber text, _dateofbirth date, _gender text)
BEGIN
	DECLARE _id INT;
    DECLARE _accid CHAR(6);
    SET _id = IFNULL((SELECT MAX(ID) FROM `OnlineAuction`.`Account`),0) + 1;
	SET _accid = CONCAT("ACC",CAST(_id DIV 100 AS CHAR),CAST(MOD(_id,100) DIV 10 AS CHAR),CAST(MOD(_id, 10) AS CHAR));
	IF ((select UserName from `OnlineAuction`.`Account` WHERE UserName = _username) IS NULL) THEN
		INSERT INTO `OnlineAuction`.`Account` (`ID`,`AccID`,`Email`,`UserName`,`Password`,`FullName`,`Address`,`PhoneNumber`,`DateOfBirth`,`Gender`)
		VALUES
		(_id,_accid,_email,_username,_password,_fullname,_address,_phonenumber,_dateofbirth,_gender);
		SELECT AccID INTO _accid FROM `OnlineAuction`.`Account` WHERE UserName = _username;
		CALL `OnlineAuction`.`proc_NewBidder` (_accid);
	ELSE 
		SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "That account already exists";
    END IF;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewCategory` (_catname TEXT)
BEGIN
	DECLARE _id INT;
	DECLARE _catid CHAR(6);
	SET _id = IFNULL((SELECT MAX(ID) FROM `OnlineAuction`.`Category`),0) + 1;
	SET _catid = CONCAT("CAT",CAST(_id DIV 100 AS CHAR),CAST(MOD(_id,100) DIV 10 AS CHAR),CAST(MOD(_id, 10) AS CHAR));
	IF (SELECT CatName FROM `OnlineAuction`.`Category` WHERE CatName = _catname) IS NULL THEN
		INSERT INTO `OnlineAuction`.`Category` (`ID`,`CatID`,`CatName`)
		VALUES
		(_id,_catid,_catname);
	ELSE 
		SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "That category name already exists!";
    END IF;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewType` (_catid CHAR(6), _typename TEXT)
BEGIN
	DECLARE _id INT;
	DECLARE _typeid CHAR(6);
	SET _id = IFNULL((SELECT MAX(ID) FROM `OnlineAuction`.`Type`),0) + 1;
	SET _typeid = CONCAT("TYP",CAST(_id DIV 100 AS CHAR),CAST(MOD(_id,100) DIV 10 AS CHAR),CAST(MOD(_id, 10) AS CHAR));
	IF (SELECT TypeName FROM `OnlineAuction`.`Type` WHERE TypeName = _typename) IS NULL THEN
		INSERT INTO `OnlineAuction`.`Type` (`ID`,`TypeID`,`TypeName`,`CatID`)
		VALUES
		(_id,_typeid,_typename,_catid);
	ELSE
		SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "That type name already exists!";
    END IF;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewProduct` (_typeid CHAR(6), _proname TEXT, _buyoutprice BIGINT, _description TEXT, _sellerid CHAR(6))
BEGIN
	DECLARE _id INT;
    DECLARE _proid CHAR(9);
	SET _id = IFNULL((SELECT MAX(ID) FROM `OnlineAuction`.`Product`),0) + 1;
	SET _proid = CONCAT("PRO",CAST(_id DIV 100000 AS CHAR),CAST(MOD(_id,100000) DIV 10000 AS CHAR),CAST(MOD(_id,10000) DIV 1000 AS CHAR),CAST(MOD(_id,1000) DIV 100 AS CHAR),CAST(MOD(_id,100) DIV 10 AS CHAR),CAST(MOD(_id, 10) AS CHAR));
    IF (SELECT ProID FROM `OnlineAuction`.`Product` WHERE ProName = _proname) IS NULL THEN
		INSERT INTO `OnlineAuction`.`Product` (`ID`,`ProID`,`TypeID`,`ProName`,`BuyoutPrice`,`Description`,`UploadDate`,`SellerID`)
		VALUES
		(_id,_proid,_typeid,_proname,_buyoutprice,_description,current_timestamp(),_sellerid);
	ELSE
		SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "That product is already added!";
    END IF;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewAuctionProduct` (_proid CHAR(9), _startingdate DATETIME, _endingdate DATETIME, _startingprice BIGINT, _pricestep BIGINT)
BEGIN
	INSERT INTO `OnlineAuction`.`AuctionProduct` (`ProID`,`StartingDate`,`EndingDate`,`StartingPrice`,`PriceStep`,`CurrentPrice`,`CurrentTopBidder`)
	VALUES
	(_proid,_startingdate,_endingdate,_startingprice,_pricestep,_startingprice,NULL);
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewWatchList` (_proid CHAR(9), _bidderid CHAR(6))
BEGIN
	INSERT INTO `OnlineAuction`.`WatchList` (`ProID`,`BidderID`)
	VALUES
	(_proid,_bidderid);
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewWaitingToBid` (_proid CHAR(9), _bidderid CHAR(6))
BEGIN
	INSERT INTO `OnlineAuction`.`WaitingToBid` (`ProID`,`BidderID`)
	VALUES
	(_proid,_bidderid);
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_AcceptedBidderReport` (_bidrepid CHAR(12))
BEGIN
	DECLARE _proid CHAR(9);
    SET _proid = (SELECT ProID FROM `OnlineAuction`.`BiddedReport` WHERE BidRepID = _bidrepid);
	UPDATE `OnlineAuction`.`BiddedReport`
	SET
		`Status` = "Denied"
	WHERE `ProID` = _proid AND `Status` = "Bidding";
    UPDATE `OnlineAuction`.`BiddedReport`
	SET
		`Status` = "Accepted"
	WHERE `BidRepID` = _bidrepid;
    UPDATE `OnlineAuction`.`AuctionProduct`
	SET
		`EndingDate` = current_timestamp(),
		`CurrentPrice` = (SELECT BidValue FROM `OnlineAuction`.`BiddedReport` WHERE BidRepID = _bidrepid),
		`CurrentTopBidder` = (SELECT BidderID FROM `OnlineAuction`.`BiddedReport` WHERE BidRepID = _bidrepid),
		`IsFinished` = TRUE
	WHERE `ProID` = (SELECT ProID FROM `OnlineAuction`.`BiddedReport` WHERE BidRepID = _bidrepid);
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewFeedbackToBidder` (_proid CHAR(9), _bidderid CHAR(6), _sellerid CHAR(6), _comment TEXT, _ratebidder INT)
BEGIN
	INSERT INTO `OnlineAuction`.`FeedbackToBidder` (`ProID`, `BidderID`, `SellerID`, `Comment`, `RateBidder`)
	VALUES
	(_proid, _bidderid, _sellerid, _comment, _ratebidder);
	UPDATE `OnlineAuction`.`Bidder`
	SET
		`BidPoint` = BidPoint + _ratebidder,
		`BeRated` = BeRated + 1,
        `RatedPoint` = (BidPoint DIV BeRated) * 10
	WHERE `BidderID` = _bidderid;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewFeedbackToSeller` (_proid CHAR(9), _bidderid CHAR(6), _sellerid CHAR(6), _comment TEXT, _rateseller INT)
BEGIN
	INSERT INTO `OnlineAuction`.`FeedbackToSeller` (`ProID`, `BidderID`, `SellerID`, `Comment`, `RateSeller`)
	VALUES
	(_proid, _bidderid, _sellerid, _comment, _rateseller);
	UPDATE `OnlineAuction`.`Seller`
	SET
		`SellPoint` = SellPoint + _rateseller,
		`BeRated` = BeRated + 1,
        `RatedPoint` = (SellPoint DIV BeRated) * 10
	WHERE `SellerID` = _sellerid;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_5SanPhamGanKetThuc` ()
BEGIN
	SELECT P.ProID, P.ProName, P.BuyoutPrice, AP.StartingDate, AP.EndingDate, AP.BidCount, AP.CurrentPrice, AP.CurrentTopBidder
	FROM `OnlineAuction`.`AuctionProduct` AS AP JOIN `OnlineAuction`.`Product` AS P ON AP.ProID = P.ProID
	WHERE AP.IsFinished = FALSE
	ORDER BY AP.EndingDate DESC LIMIT 5;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_5SanPhamRaGiaNhieuNhat` ()
BEGIN
	SELECT P.ProID, P.ProName, P.BuyoutPrice, AP.StartingDate, AP.EndingDate, AP.BidCount, AP.CurrentPrice, AP.CurrentTopBidder
	FROM `OnlineAuction`.`AuctionProduct` AS AP JOIN `OnlineAuction`.`Product` AS P ON AP.ProID = P.ProID
	WHERE AP.IsFinished = FALSE
	ORDER BY AP.BidCount DESC LIMIT 5;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_5SanPhamGiaCaoNhat` ()
BEGIN
	SELECT P.ProID, P.ProName, P.BuyoutPrice, AP.StartingDate, AP.EndingDate, AP.BidCount, AP.CurrentPrice, AP.CurrentTopBidder
	FROM `OnlineAuction`.`AuctionProduct` AS AP JOIN `OnlineAuction`.`Product` AS P ON AP.ProID = P.ProID
	WHERE AP.IsFinished = FALSE
	ORDER BY AP.CurrentPrice DESC LIMIT 5;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewWaitingToSell` (_accid CHAR(6))
BEGIN
	IF (SELECT AccID FROM `OnlineAuction`.`Seller` WHERE AccID = _accid) IS NULL THEN
		INSERT INTO `OnlineAuction`.`WaitingToSell`(`AccID`,`DateTime`)
		VALUES
		(_accid,current_timestamp());
    ELSE
		SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "That account is already a seller!";
	END IF;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NeedToBid`()
BEGIN
	SELECT * FROM
	(SELECT * FROM
	(SELECT T1.`ProID`,T1.`StartingDate`,T1.`EndingDate`,T1.`StartingPrice`,T1.`PriceStep`,T1.`CurrentPrice`,T1.`CurrentTopBidder`,T1.`BidCount`,T1.`IsFinished`,
    `Product`.`TypeID`,`Product`.`ProName`,`Product`.`BuyoutPrice`,`Product`.`Description`,`Product`.`UploadDate`,`Product`.`SellerID` FROM
	(SELECT * FROM `OnlineAuction`.`AuctionProduct` WHERE IsFinished = FALSE) AS T1
    JOIN `OnlineAuction`.`Product` ON T1.ProID = Product.ProID) AS T2 LEFT JOIN `OnlineAuction`.`Bidder` ON T2.CurrentTopBidder = Bidder.BidderID) AS T3 LEFT JOIN `OnlineAuction`.`Account` ON T3.AccID = Account.AccID;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_SanPhamDangDauGiaTheoBidderID` (_bidderid CHAR(6))
BEGIN
	SELECT T7.`BidRepID`,T7.`ProID`,T7.`BidderID`,T7.`BidValue`,T7.`BidDate`,T7.`Status`,
	T7.`StartingDate`,T7.`EndingDate`,T7.`StartingPrice`,T7.`PriceStep`,T7.`CurrentPrice`,T7.`CurrentTopBidder`,T7.`BidCount`,T7.`IsFinished`,
    T7.`TypeID`,T7.`ProName`,T7.`BuyoutPrice`,T7.`Description`,T7.`UploadDate`,T7.`SellerID`,
    T7.`BidPoint`,T7.`BeRated`,T7.`RatedPoint`,T7.`AccID`,
    T8.`Email`,T8.`UserName`,T8.`Password`,T8.`FullName`,T8.`Address`,T8.`PhoneNumber`,T8.`DateOfBirth`,T8.`Gender` FROM
	(SELECT T5.`BidRepID`,T5.`ProID`,T5.`BidderID`,T5.`BidValue`,T5.`BidDate`,T5.`Status`,
	T5.`StartingDate`,T5.`EndingDate`,T5.`StartingPrice`,T5.`PriceStep`,T5.`CurrentPrice`,T5.`CurrentTopBidder`,T5.`BidCount`,T5.`IsFinished`,
    T5.`TypeID`,T5.`ProName`,T5.`BuyoutPrice`,T5.`Description`,T5.`UploadDate`,T5.`SellerID`,
    T6.`BidPoint`,T6.`BeRated`,T6.`RatedPoint`,T6.`AccID` FROM
	(SELECT T3.`BidRepID`,T3.`ProID`,T3.`BidderID`,T3.`BidValue`,T3.`BidDate`,T3.`Status`,
	T3.`StartingDate`,T3.`EndingDate`,T3.`StartingPrice`,T3.`PriceStep`,T3.`CurrentPrice`,T3.`CurrentTopBidder`,T3.`BidCount`,T3.`IsFinished`,
    T4.`TypeID`,T4.`ProName`,T4.`BuyoutPrice`,T4.`Description`,T4.`UploadDate`,T4.`SellerID` FROM
	(SELECT T1.`BidRepID`,T1.`ProID`,T1.`BidderID`,T1.`BidValue`,T1.`BidDate`,T1.`Status`,
	T2.`StartingDate`,T2.`EndingDate`,T2.`StartingPrice`,T2.`PriceStep`,T2.`CurrentPrice`,T2.`CurrentTopBidder`,T2.`BidCount`,T2.`IsFinished` FROM
	(SELECT * FROM `OnlineAuction`.`BiddedReport` WHERE BidderID = _bidderid AND Status = "Bidding") AS T1
	JOIN `OnlineAuction`.`AuctionProduct` AS T2 ON T1.ProID = T2.ProID) AS T3 
	JOIN `OnlineAuction`.`Product` AS T4 ON T3.ProID = T4.ProID) AS T5
	JOIN `OnlineAuction`.`Bidder` AS T6 ON T5.BidderID = T6.BidderID) AS T7
	JOIN `OnlineAuction`.`Account` AS T8 ON T7.AccID = T8.AccID;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_UpdateFinishedAuctionProduct`()
BEGIN
	SET SQL_SAFE_UPDATES = 0;
	UPDATE `OnlineAuction`.`AuctionProduct`
	SET `IsFinished` = TRUE
	WHERE `ProID` IN (SELECT T.`ProID` FROM (SELECT `AuctionProduct`.`ProID`
	FROM `OnlineAuction`.`AuctionProduct` WHERE timestampdiff(SECOND,current_timestamp(),`EndingDate`) < 0 AND `IsFinished` = FALSE) AS T);
    UPDATE `OnlineAuction`.`BiddedReport`
	SET
	`Status` = "Accepted"
	WHERE (`ProID`,`BidderID`) IN (SELECT `ProID`,`CurrentTopBidder` FROM `OnlineAuction`.`AuctionProduct` WHERE `IsFinished` = TRUE) AND `Status` = "Bidding";
	UPDATE `OnlineAuction`.`BiddedReport`
	SET
	`Status` = "Denied"
	WHERE `ProID` IN (SELECT `ProID` FROM `OnlineAuction`.`AuctionProduct` WHERE `IsFinished` = TRUE) AND `Status` = "Bidding";
    SET SQL_SAFE_UPDATES = 1;
END $$
DELIMITER ;

-- event
-- SHOW PROCESSLIST;
SET GLOBAL event_scheduler = ON;
-- DROP EVENT IF EXISTS event_AutoFinishAuctionProduct;
DELIMITER $$
CREATE EVENT event_AutoFinishAuctionProduct
ON SCHEDULE EVERY 1 MINUTE STARTS current_timestamp()
DO
BEGIN
	CALL `proc_UpdateFinishedAuctionProduct`();
END$$
DELIMITER ;
-- SHOW EVENTS FROM OnlineAuction;

DELIMITER $$
CREATE PROCEDURE `proc_RemoveType`(_typeid CHAR(6))
BEGIN
	SET FOREIGN_KEY_CHECKS=0;
	DELETE FROM `OnlineAuction`.`Type`
	WHERE `TypeID` = _typeid;
    SET FOREIGN_KEY_CHECKS=1;
END $$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_UpdateType`(_typeid CHAR(6), _typename TEXT, _catid CHAR(6))
BEGIN
	SET FOREIGN_KEY_CHECKS=0;
	UPDATE `OnlineAuction`.`Type`
	SET
    `TypeName` = _typename,
	`CatID` = _catid
	WHERE `TypeID` = _typeid;
    SET FOREIGN_KEY_CHECKS=1;
END $$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER trigger_AfterDeleteType
AFTER DELETE ON `OnlineAuction`.`Type` FOR EACH ROW
BEGIN
	DECLARE _delcatid CHAR(6);
    SET _delcatid = (SELECT T3.CatID FROM (SELECT T1.CatID, count(T2.TypeID) AS count
	FROM `OnlineAuction`.`Category` AS T1 LEFT JOIN `OnlineAuction`.`Type` AS T2 ON T1.CatID = T2.CatID
    GROUP BY T1.CatID) AS T3 WHERE T3.count = 0);
    DELETE FROM `OnlineAuction`.`Category`
	WHERE `CatID` = _delcatid;
END$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER trigger_AfterUpdateType
AFTER UPDATE ON `OnlineAuction`.`Type` FOR EACH ROW
BEGIN
	DECLARE _delcatid CHAR(6);
    SET _delcatid = (SELECT T3.CatID FROM (SELECT T1.CatID, count(T2.TypeID) AS count
	FROM `OnlineAuction`.`Category` AS T1 LEFT JOIN `OnlineAuction`.`Type` AS T2 ON T1.CatID = T2.CatID
    GROUP BY T1.CatID) AS T3 WHERE T3.count = 0);
    DELETE FROM `OnlineAuction`.`Category`
	WHERE `CatID` = _delcatid;
END$$
DELIMITER ;

-- ------------------------------------------------
DROP PROCEDURE IF EXISTS `OnlineAuction`.`proc_NewBiddedReportWithAutoBid`;
DROP PROCEDURE IF EXISTS `OnlineAuction`.`proc_NewBiddedReport`;
DROP PROCEDURE IF EXISTS `OnlineAuction`.`proc_NewAutoBid`;
DELIMITER $$
CREATE PROCEDURE `proc_NewAutoBid`(_bidderid CHAR(6), _proid CHAR(9), _maxprice BIGINT)
BEGIN
	DECLARE _prebidder CHAR(6);
    DECLARE _premaxprice BIGINT;
	DECLARE _temp BIGINT;
	SET _temp = (SELECT `AuctionProduct`.`CurrentPrice` + `AuctionProduct`.`PriceStep` FROM `OnlineAuction`.`AuctionProduct` WHERE `ProID` = _proid);
    SET _premaxprice = (SELECT `AutoBid`.`MaxPrice` FROM `OnlineAuction`.`AutoBid` WHERE `ProID` = _proid);
    SET _prebidder = (SELECT `AutoBid`.`BidderID` FROM `OnlineAuction`.`AutoBid` WHERE `ProID` = _proid);
	IF (SELECT `AutoBid`.`BidderID` FROM `OnlineAuction`.`AutoBid` WHERE `ProID` = _proid) IS NULL THEN
        IF (SELECT `AuctionProduct`.`CurrentPrice` FROM `OnlineAuction`.`AuctionProduct` WHERE `ProID` = _proid) < _maxprice THEN
			INSERT INTO `OnlineAuction`.`AutoBid`
			(`BidderID`,`ProID`,`MaxPrice`)
			VALUES
			(_bidderid,_proid,_maxprice);
			IF (SELECT `AuctionProduct`.`CurrentTopBidder` FROM `OnlineAuction`.`AuctionProduct` WHERE `ProID` = _proid) <> _bidderid THEN
				CALL `OnlineAuction`.`proc_NewBiddedReport`(_proid, _bidderid, _temp);
			END IF;
		ELSE
			SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "You must set a higher value.";
        END IF;
	ELSE
		IF (SELECT `AutoBid`.`BidderID` FROM `OnlineAuction`.`AutoBid` WHERE `ProID` = _proid) = _bidderid THEN
			IF (SELECT `AuctionProduct`.`CurrentPrice` FROM `OnlineAuction`.`AuctionProduct` WHERE `ProID` = _proid) < _maxprice THEN
				UPDATE `OnlineAuction`.`AutoBid`
				SET
				`MaxPrice` = _maxprice
				WHERE `BidderID` = _bidderid AND `ProID` = _proid;
			ELSE
				SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "You must set a higher value.";
			END IF;
		ELSE
			IF (SELECT `AutoBid`.`MaxPrice` FROM `OnlineAuction`.`AutoBid` WHERE `ProID` = _proid) < _maxprice THEN
				INSERT INTO `OnlineAuction`.`AutoBid`
				(`BidderID`,`ProID`,`MaxPrice`)
				VALUES
				(_bidderid,_proid,_maxprice);
                DELETE FROM `OnlineAuction`.`AutoBid`
				WHERE `BidderID` = _prebidder;
				CALL `OnlineAuction`.`proc_NewBiddedReport`(_proid, _prebidder, _premaxprice);
			ELSE
				SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "You must set a higher value.";
			END IF;
		END IF;
	END IF;
END $$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewBiddedReportTemp` (_proid CHAR(9), _bidderid CHAR(6), _bidvalue BIGINT)
BEGIN
	DECLARE _id INT;
    DECLARE _bidrepid CHAR(12);
    SET _id = IFNULL((SELECT MAX(ID) FROM `OnlineAuction`.`BiddedReport`),0) + 1;
	SET _bidrepid = CONCAT("BRP",CAST(_id DIV 100000000 AS CHAR),CAST(MOD(_id,100000000) DIV 10000000 AS CHAR),CAST(MOD(_id,10000000) DIV 1000000 AS CHAR),CAST(MOD(_id,1000000) DIV 100000 AS CHAR),CAST(MOD(_id,100000) DIV 10000 AS CHAR),CAST(MOD(_id,10000) DIV 1000 AS CHAR),CAST(MOD(_id,1000) DIV 100 AS CHAR),CAST(MOD(_id,100) DIV 10 AS CHAR),CAST(MOD(_id, 10) AS CHAR));
    IF (SELECT `ProID` FROM `OnlineAuction`.`AuctionProduct` WHERE ProID = _proid) IS NULL THEN
		SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "Product is not being auctioning!";
    ELSE
		IF (SELECT `ProID` FROM `OnlineAuction`.`AuctionProduct` WHERE ProID = _proid AND IsFinished = FALSE) IS NULL THEN
			SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "Product had been finished auctioning!";
        ELSE
			IF (SELECT `CurrentTopBidder` FROM `OnlineAuction`.`AuctionProduct` WHERE ProID = _proid) = _bidderid THEN
				IF (SELECT `CurrentPrice` FROM `OnlineAuction`.`AuctionProduct` WHERE ProID = _proid) < _bidvalue THEN
					UPDATE `OnlineAuction`.`BiddedReport`
					SET
					`Status` = "Cancel"
					WHERE `ProID` = _proid AND `BidderID` = _bidderid;
					INSERT INTO `OnlineAuction`.`BiddedReport`(`ID`,`BidRepID`,`ProID`,`BidderID`,`BidValue`,`BidDate`,`Status`)
					VALUES
					(_id,_bidrepid,_proid,_bidderid,_bidvalue,current_timestamp(),"Bidding");
                    UPDATE `OnlineAuction`.`AuctionProduct`
					SET
					`CurrentPrice` = _bidvalue,
					`CurrentTopBidder` = _bidderid,
					`BidCount` = (SELECT count(*) FROM `OnlineAuction`.`BiddedReport` WHERE `ProID` = _proid AND `Status` = "Bidding")
					WHERE `ProID` = _proid;
                    UPDATE `OnlineAuction`.`AuctionProduct`
					SET
					`EndingDate` = date_add(EndingDate, INTERVAL 10 MINUTE)
					WHERE `ProID` = _proid AND timestampdiff(MINUTE,current_timestamp(),EndingDate) < 6;
                ELSE
					SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "You must bid with a higher value!";
                END IF;
            ELSE
				IF (SELECT `CurrentPrice` FROM `OnlineAuction`.`AuctionProduct` WHERE ProID = _proid) > _bidvalue THEN
					SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = "You must bid with a higher value!";
                ELSE
					IF (SELECT `CurrentPrice` FROM `OnlineAuction`.`AuctionProduct` WHERE ProID = _proid) = _bidvalue THEN
						UPDATE `OnlineAuction`.`BiddedReport`
						SET
						`Status` = "Cancel"
						WHERE `ProID` = _proid AND `BidderID` = _bidderid;
						INSERT INTO `OnlineAuction`.`BiddedReport`(`ID`,`BidRepID`,`ProID`,`BidderID`,`BidValue`,`BidDate`,`Status`)
						VALUES
						(_id,_bidrepid,_proid,_bidderid,_bidvalue,current_timestamp(),"Bidding");
						UPDATE `OnlineAuction`.`AuctionProduct`
						SET
						`BidCount` = (SELECT count(*) FROM `OnlineAuction`.`BiddedReport` WHERE `ProID` = _proid AND `Status` = "Bidding")
						WHERE `ProID` = _proid;
                        UPDATE `OnlineAuction`.`AuctionProduct`
						SET
						`CurrentTopBidder` = _bidderid
						WHERE `ProID` = _proid AND `CurrentTopBidder` IS NULL;
						UPDATE `OnlineAuction`.`AuctionProduct`
						SET
						`EndingDate` = date_add(EndingDate, INTERVAL 10 MINUTE)
						WHERE `ProID` = _proid AND timestampdiff(MINUTE,current_timestamp(),EndingDate) < 6;
                    ELSE
						UPDATE `OnlineAuction`.`BiddedReport`
						SET
						`Status` = "Cancel"
						WHERE `BidderID` = _bidderid AND `ProID` = _proid;
						INSERT INTO `OnlineAuction`.`BiddedReport`(`ID`,`BidRepID`,`ProID`,`BidderID`,`BidValue`,`BidDate`,`Status`)
						VALUES
						(_id,_bidrepid,_proid,_bidderid,_bidvalue,current_timestamp(),"Bidding");
						UPDATE `OnlineAuction`.`AuctionProduct`
						SET
						`CurrentPrice` = _bidvalue,
						`CurrentTopBidder` = _bidderid,
						`BidCount` = (SELECT count(*) FROM `OnlineAuction`.`BiddedReport` WHERE `ProID` = _proid AND `Status` = "Bidding")
						WHERE `ProID` = _proid;
						UPDATE `OnlineAuction`.`AuctionProduct`
						SET
						`EndingDate` = date_add(EndingDate, INTERVAL 10 MINUTE)
						WHERE `ProID` = _proid AND timestampdiff(MINUTE,current_timestamp(),EndingDate) < 6;
                    END IF;
                END IF;
            END IF;
        END IF;
    END IF;
END$$
DELIMITER ;
DELIMITER $$
CREATE PROCEDURE `proc_NewBiddedReport`(_proid CHAR(9), _bidderid CHAR(6), _bidvalue BIGINT)
BEGIN
	DECLARE _prebidderid CHAR(6);
    DECLARE _prebidcount INT UNSIGNED;
    SET _prebidderid = (SELECT `AuctionProduct`.`CurrentTopBidder` FROM `OnlineAuction`.`AuctionProduct` WHERE `ProID` = _proid);
    SET _prebidcount = (SELECT `AuctionProduct`.`BidCount` FROM `OnlineAuction`.`AuctionProduct` WHERE `ProID` = _proid);
    CALL `OnlineAuction`.`proc_NewBiddedReportTemp`(_proid,_bidderid,_bidvalue);
    IF ((SELECT `AuctionProduct`.`BidCount` FROM `OnlineAuction`.`AuctionProduct` WHERE `ProID` = _proid) > _prebidcount) THEN
		IF ((_prebidderid,_proid) IN (SELECT `AutoBid`.`BidderID`,`AutoBid`.`ProID` FROM `OnlineAuction`.`AutoBid`)) THEN
			IF (_bidvalue + (SELECT `AuctionProduct`.`PriceStep` FROM `OnlineAuction`.`AuctionProduct` WHERE `ProID` = _proid)) > (SELECT `AutoBid`.`MaxPrice` FROM `OnlineAuction`.`AutoBid` WHERE `ProID` = _proid AND `BidderID` = _prebidderid) THEN
				DELETE FROM `OnlineAuction`.`AutoBid` WHERE `ProID` = _proid AND `BidderID` = _prebidderid;
            ELSE
				CALL `OnlineAuction`.`proc_NewBiddedReport`(_proid,_prebidderid,_bidvalue + (SELECT `AuctionProduct`.`PriceStep`
				FROM `OnlineAuction`.`AuctionProduct` WHERE `ProID` = _proid));
            END IF;
		END IF;	
    END IF;
END $$
DELIMITER ;