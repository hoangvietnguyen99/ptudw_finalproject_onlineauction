/*create database*/
DROP DATABASE IF EXISTS `OnlineAuction`;
CREATE DATABASE `OnlineAuction` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

USE OnlineAuction;

DROP TABLE IF EXISTS `OnlineAuction`.`Account`;
CREATE TABLE `OnlineAuction`.`Account` (
  `ID` INT NOT NULL,
  `AccID` CHAR(6) NOT NULL,
  `Email` TEXT NOT NULL,
  `UserName` TEXT NOT NULL,
  `Password` TEXT NOT NULL,
  `FullName` TEXT,
  `Address` TEXT,
  `PhoneNumber` TEXT,
  `DateOfBirth` DATE,
  `Gender` TEXT,
  PRIMARY KEY `pk_AccID`(`AccID`)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`Bidder`;
CREATE TABLE `OnlineAuction`.`Bidder` (
  `ID` INT NOT NULL,
  `BidderID` CHAR(6) NOT NULL,
  `BidPoint` INT DEFAULT 0,
  `BeRated` INT DEFAULT 0,
  `RatedPoint` FLOAT DEFAULT 0.00,
  `AccID` CHAR(6) UNIQUE,
  PRIMARY KEY `pk_Bidder_BidderID`(`BidderID`),
  CONSTRAINT `fk_Bidder_Account_AccID` FOREIGN KEY (AccID) REFERENCES `OnlineAuction`.`Account` (AccID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`Seller`;
CREATE TABLE `OnlineAuction`.`Seller` (
  `ID` INT NOT NULL,
  `SellerID` CHAR(6) NOT NULL,
  `SellPoint` INT DEFAULT 0,
  `BeRated` INT DEFAULT 0,
  `RatedPoint` FLOAT DEFAULT 0.00,
  `AccID` CHAR(6) UNIQUE,
  PRIMARY KEY `pk_Seller_SellerID`(`SellerID`),
  CONSTRAINT `fk_Seller_Account_AccID` FOREIGN KEY (AccID) REFERENCES `OnlineAuction`.`Account` (AccID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`Category`;
CREATE TABLE `OnlineAuction`.`Category` (
  `ID` INT NOT NULL,
  `CatID` CHAR(6) NOT NULL,
  `CatName` TEXT NOT NULL,
  PRIMARY KEY `pk_Category_CatID`(`CatID`)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`Type`;
CREATE TABLE `OnlineAuction`.`Type` (
  `ID` INT NOT NULL,
  `TypeID` CHAR(6) NOT NULL,
  `TypeName` TEXT NOT NULL,
  `CatID` CHAR(6) NOT NULL,
  PRIMARY KEY `pk_Type_TypeID`(`TypeID`),
  CONSTRAINT `fk_Type_Category_CatID` FOREIGN KEY (CatID) REFERENCES `OnlineAuction`.`Category` (CatID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`Product`;
CREATE TABLE `OnlineAuction`.`Product` (
  `ID` INT NOT NULL,
  `ProID` CHAR(9) NOT NULL,
  `TypeID` CHAR(6) NOT NULL,
  `ProName` TEXT NOT NULL,
  `BuyoutPrice` BIGINT,
  `Description` TEXT,
  `UploadDate` DATETIME,
  `SellerID` CHAR(6) NOT NULL,
  PRIMARY KEY `pk_Product_ProID`(`ProID`),
  CONSTRAINT `fk_Product_Seller_SellerID` FOREIGN KEY (SellerID) REFERENCES `OnlineAuction`.`Seller` (SellerID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`AuctionProduct`;
CREATE TABLE `OnlineAuction`.`AuctionProduct` (
  `ProID` CHAR(9) NOT NULL,
  `StartingDate` DATETIME,
  `EndingDate` DATETIME,
  `StartingPrice` BIGINT,
  `PriceStep` BIGINT,
  `CurrentPrice` BIGINT,
  `CurrentTopBidder` CHAR(6),
  `BidCount` INT UNSIGNED DEFAULT 0,
  `IsFinished` BOOL DEFAULT FALSE,
  PRIMARY KEY `pk_AuctionProduct_ProID`(`ProID`),
  CONSTRAINT `fk_AuctionProduct_Product_ProID` FOREIGN KEY (ProID) REFERENCES `OnlineAuction`.`Product` (ProID),
  CONSTRAINT `fk_AuctionProduct_Bidder_CurrentTopBidder` FOREIGN KEY (CurrentTopBidder) REFERENCES `OnlineAuction`.`Bidder` (BidderID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`WatchList`;
CREATE TABLE `OnlineAuction`.`WatchList` (
  `ProID` CHAR(9),
  `BidderID` CHAR(6),
  PRIMARY KEY `pk_WatchList_ProID_BidderID`(`ProID`, `BidderID`),
  CONSTRAINT `fk_WatchList_AuctionProduct_ProID` FOREIGN KEY (ProID) REFERENCES `OnlineAuction`.`AuctionProduct` (ProID),
  CONSTRAINT `fk_WatchList_Bidder_BidderID` FOREIGN KEY (BidderID) REFERENCES `OnlineAuction`.`Bidder` (BidderID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`WaitingToBid`;
CREATE TABLE `OnlineAuction`.`WaitingToBid` (
  `ProID` CHAR(9),
  `BidderID` CHAR(6),
  `SellerAccept` BOOL DEFAULT FALSE,
  PRIMARY KEY `pk_WaitingToBid_ProID_BidderID`(`ProID`, `BidderID`),
  CONSTRAINT `fk_WaitingToBid_AuctionProduct_ProID` FOREIGN KEY (ProID) REFERENCES `OnlineAuction`.`AuctionProduct` (ProID),
  CONSTRAINT `fk_WaitingToBid_Bidder_BidderID` FOREIGN KEY (BidderID) REFERENCES `OnlineAuction`.`Bidder` (BidderID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`BiddedReport`;
CREATE TABLE `OnlineAuction`.`BiddedReport` (
  `ID` INT NOT NULL,
  `BidRepID` CHAR(12) NOT NULL,
  `ProID` CHAR(9),
  `BidderID` CHAR(6),
  `BidValue` BIGINT,
  `BidDate` DATETIME,
  `Status` TEXT,
  PRIMARY KEY `pk_BiddedReport_BidRepID`(`BidRepID`),
  CONSTRAINT `fk_BiddedReport_AuctionProduct_ProID` FOREIGN KEY (ProID) REFERENCES `OnlineAuction`.`AuctionProduct` (ProID),
  CONSTRAINT `fk_BiddedReport_Bidder_BidderID` FOREIGN KEY (BidderID) REFERENCES `OnlineAuction`.`Bidder` (BidderID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`FeedbackToBidder`;
CREATE TABLE `OnlineAuction`.`FeedbackToBidder` (
  `ProID` CHAR(9),
  `BidderID` CHAR(6),
  `SellerID` CHAR(6),
  `Comment` TEXT,
  `RateBidder` INT NOT NULL,
  PRIMARY KEY `pk_FeedbackToBidder_ProID`(`ProID`),
  CONSTRAINT `fk_FeedbackToBidder_AuctionProduct_ProID` FOREIGN KEY (ProID) REFERENCES `OnlineAuction`.`AuctionProduct` (ProID),
  CONSTRAINT `fk_FeedbackToBidder_Bidder_BidderID` FOREIGN KEY (BidderID) REFERENCES `OnlineAuction`.`Bidder` (BidderID),
  CONSTRAINT `fk_FeedbackToBidder_Seller_SellerID` FOREIGN KEY (SellerID) REFERENCES `OnlineAuction`.`Seller` (SellerID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`FeedbackToSeller`;
CREATE TABLE `OnlineAuction`.`FeedbackToSeller` (
  `ProID` CHAR(9),
  `BidderID` CHAR(6),
  `SellerID` CHAR(6),
  `Comment` TEXT,
  `RateSeller` INT NOT NULL,
  PRIMARY KEY `pk_FeedbackToSeller_ProID`(`ProID`),
  CONSTRAINT `fk_FeedbackToSeller_AuctionProduct_ProID` FOREIGN KEY (ProID) REFERENCES `OnlineAuction`.`AuctionProduct` (ProID),
  CONSTRAINT `fk_FeedbackToSeller_Bidder_BidderID` FOREIGN KEY (BidderID) REFERENCES `OnlineAuction`.`Bidder` (BidderID),
  CONSTRAINT `fk_FeedbackToSeller_Seller_SellerID` FOREIGN KEY (SellerID) REFERENCES `OnlineAuction`.`Seller` (SellerID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`WaitingToSell`;
CREATE TABLE `OnlineAuction`.`WaitingToSell` (
  `AccID` CHAR(6),
  `DateTime` DATETIME,
  PRIMARY KEY `pk_WaitingToSell_AccID`(`AccID`),
  CONSTRAINT `fk_WaitingToSell_Account_AccID` FOREIGN KEY (AccID) REFERENCES `OnlineAuction`.`Account` (AccID)
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`Admin`;
CREATE TABLE `OnlineAuction`.`Admin` (
  `UserName` TEXT,
  `Password` TEXT
) ENGINE = InnoDB;

DROP TABLE IF EXISTS `OnlineAuction`.`AutoBid`;
CREATE TABLE `OnlineAuction`.`AutoBid` (
  `BidderID` char(6) NOT NULL,
  `ProID` char(9) NOT NULL,
  `MaxPrice` BIGINT,
  PRIMARY KEY `pk_AutoBid_BidderID_ProID`(BidderID,ProID),
  CONSTRAINT `fk_AutoBid_Bidder_BidderID` FOREIGN KEY (BidderID) REFERENCES `OnlineAuction`.`Bidder` (BidderID),
  CONSTRAINT `fk_AutoBid_AuctionProduct` FOREIGN KEY (ProID) REFERENCES `OnlineAuction`.`AuctionProduct` (ProID)
) ENGINE = InnoDB;

INSERT INTO `OnlineAuction`.`Admin`
(`UserName`,
`Password`)
VALUES
('ADMIN@onlineauction.com', 'ef48f3dacfa2334779454afbc508fd4249d5f72a799d67c7d7a9e8db77b928a016f68344c98');